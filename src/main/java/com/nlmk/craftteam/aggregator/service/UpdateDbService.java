package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class UpdateDbService {
    @Value("#{'${car.hosts}'.split(',')}")
    private List<String> hosts;

    private final CarService carService;

    private final CarAggregateService carAggregateService;

    private final CarModelService carModelService;

    private final CarBrandService carBrandService;

    public UpdateDbService(
            CarService carService,
            CarAggregateService carAggregateService,
            CarModelService carModelService,
            CarBrandService carBrandService) {
        this.carService = carService;
        this.carAggregateService = carAggregateService;
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @Scheduled(fixedRate = 60000)
    public void launchUpdateDb() {
        log.info("Start update. Current time is :: " + LocalDateTime.now());
        for (String host : hosts) {
            ResponseEntity<CarBrand[]> brands = new RestTemplate().getForEntity(host + "/brands", CarBrand[].class);
            List<CarBrand> carBrandsNew = carBrandService.saveAllNew(brands.getBody());

            for (CarBrand carBrand : carBrandsNew) {
                ResponseEntity<CarModel[]> models = new RestTemplate().getForEntity(host + "/models/{brandId}", CarModel[].class, carBrand.getId());

                List<CarModel> modelsBody = Arrays.stream(models.getBody()).map(mod ->
                        CarModel.builder().carBrand(carBrand).name(mod.getName()).build()
                ).collect(Collectors.toList());

                carAggregateService.save(modelsBody, host);
            }

        }
        log.info("Finished update. Current time is :: " + LocalDateTime.now());
    }
}