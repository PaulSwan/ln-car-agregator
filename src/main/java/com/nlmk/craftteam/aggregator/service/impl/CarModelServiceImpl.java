package com.nlmk.craftteam.aggregator.service.impl;


import com.nlmk.craftteam.aggregator.repository.CarModelRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import com.nlmk.craftteam.aggregator.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class CarModelServiceImpl implements CarModelService {

    private CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    @Override
    public List<CarModel> save(CarModel[] body) {
        return carModelRepository.saveAll(Arrays.asList(body));
    }

}
