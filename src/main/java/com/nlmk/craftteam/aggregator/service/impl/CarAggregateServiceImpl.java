package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.repository.CarAggregateRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import com.nlmk.craftteam.aggregator.service.CarAggregateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarAggregateServiceImpl implements CarAggregateService {

    private CarAggregateRepository carAggregateRepository;

    @Autowired
    public CarAggregateServiceImpl(CarAggregateRepository carAggregateRepository) {
        this.carAggregateRepository = carAggregateRepository;
    }

    @Override
    public List<CarAggregate> save(List<CarModel> modelsBody, String host) {
        if (host == null || host.isEmpty()) return new ArrayList<>();
        List<CarAggregate> carAggregates = modelsBody.stream()
                .map(body -> CarAggregate.builder()
                        .carModel(body)
                        .host(host)
                        .lastUpdateTime(LocalDateTime.now())
                        .build()
                ).collect(Collectors.toList());
        return carAggregateRepository.saveAll(carAggregates);
    }

    @Override
    public List<CarModel> getAllUnique() {
        return carAggregateRepository.findAll().stream()
                .collect(Collectors.groupingBy(f -> f.getCarModel(), Collectors.counting()))
                .entrySet().stream()
                .map(e -> e.getKey())
                .collect(Collectors.toList());
    }
}
