package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.repository.CarBrandRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.service.CarBrandService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarBrandServiceImpl implements CarBrandService {

    private final CarBrandRepository carBrandRepository;

    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrand> saveAllNew(CarBrand[] body) {
        return carBrandRepository.saveAll(
                Arrays.stream(body)
                        .filter(this::isNew)
                        .map(c -> CarBrand.builder()
                                .name(c.getName())
                                .build()).collect(Collectors.toList())
        );
    }

    private boolean isNew(CarBrand brand) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("id")
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.ignoreCase());
        return !carBrandRepository.exists(Example.of(brand, matcher));
    }
}