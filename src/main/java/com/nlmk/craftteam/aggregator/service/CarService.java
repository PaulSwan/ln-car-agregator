package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;

public interface CarService {
    CarBrand[] save(CarBrand[] body);
}
