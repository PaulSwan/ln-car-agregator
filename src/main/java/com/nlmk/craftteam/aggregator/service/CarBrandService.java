package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;

import java.util.List;

public interface CarBrandService {
    List<CarBrand> saveAllNew(CarBrand[] body);
}
