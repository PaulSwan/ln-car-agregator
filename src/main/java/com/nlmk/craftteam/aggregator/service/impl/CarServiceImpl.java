package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.repository.CarAggregateRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarServiceImpl implements CarService {

    private CarAggregateRepository carAggregateRepository;

    @Autowired
    public CarServiceImpl(CarAggregateRepository carAggregateRepository) {
        this.carAggregateRepository = carAggregateRepository;
    }

    @Override
    public CarBrand[] save(CarBrand[] body) {
        List<CarAggregate> all = carAggregateRepository.findAll();
        return new CarBrand[0];
    }
}
