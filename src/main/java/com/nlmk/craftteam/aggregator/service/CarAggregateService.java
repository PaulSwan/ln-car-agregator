package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;

import java.util.List;

public interface CarAggregateService {
    List<CarAggregate> save(List<CarModel> modelsBody, String host);
    List<CarModel> getAllUnique();
}
