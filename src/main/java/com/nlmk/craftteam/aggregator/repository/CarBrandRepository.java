package com.nlmk.craftteam.aggregator.repository;

import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {

}
