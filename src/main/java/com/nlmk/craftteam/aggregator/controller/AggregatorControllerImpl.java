package com.nlmk.craftteam.aggregator.controller;

import com.nlmk.craftteam.aggregator.service.CarAggregateService;
import com.nlmk.craftteam.aggregator.service.CarBrandService;
import com.nlmk.craftteam.aggregator.service.CarModelService;
import com.nlmk.craftteam.aggregator.service.CarService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;


@RestController
@RequestMapping("/")
@Transactional
public class AggregatorControllerImpl implements AggregatorController {

    private final CarService carService;

    private final CarAggregateService carAggregateService;

    private final CarModelService carModelService;

    private final CarBrandService carBrandService;

    public AggregatorControllerImpl(
            CarService carService,
            CarAggregateService carAggregateService,
            CarModelService carModelService,
            CarBrandService carBrandService) {
        this.carService = carService;
        this.carAggregateService = carAggregateService;
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @Override
    @GetMapping("/aggregator")
    public ModelAndView getAllBrandsAndModels() {
        ModelAndView model = new ModelAndView();
        model.setViewName("aggregator");
        model.addObject("data", carAggregateService.getAllUnique());
        return model;
    }
}